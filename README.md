# README #

###This is simple driving licence quiz Android application. 
It checks the state of checkboxes, radio buttons and content of text area. 
Counts it all up and show the result on the bottom after click on submit button.

Is compatible with Android lollipop and older versions

I created it as final task on Udacity course 'EU Scholarship - Android Development for Beginners 
---
![Screenshot_small.png](https://bitbucket.org/repo/5qxpMK7/images/1021122994-Screenshot_small.png)