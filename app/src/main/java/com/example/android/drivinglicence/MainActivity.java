package com.example.android.drivinglicence;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    int total_points;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public int getRadioPoints(View view){
        int radioPoints = 0;
        //get true or false value if radio button is checked or not
        RadioButton radio3 = (RadioButton) findViewById(R.id.radio_3);
        boolean checkedRadio3 = radio3.isChecked();
        if (checkedRadio3){
            radioPoints += 1;
        }
        RadioButton radio4 = (RadioButton) findViewById(R.id.radio_4);
        boolean checkedRadio4 = radio4.isChecked();
        if (checkedRadio4){
            radioPoints += 1;
        }
        RadioButton radio11 = (RadioButton) findViewById(R.id.radio_11);
        boolean checkedRadio11 = radio11.isChecked();
        if (checkedRadio11){
            radioPoints += 1;
        }
        return radioPoints;
    }

    public int getTextPoints(View view){
        int textPoints = 0;
        //get text from editable text field
        EditText inputText = (EditText) findViewById(R.id.lights);
        String lightsInput = inputText.getText().toString();
        //compare value of the text field with expected answer ignoring letter capitalization
        if(lightsInput.equalsIgnoreCase("yes")){
            textPoints += 1;
        }
        return textPoints;
    }

    public int getCheckBoxPoints() {
        int checkBoxPoints = 0;

        //get true or false value is checkbox checked or not
        //checkbox for compensation
        CheckBox compensation = (CheckBox) findViewById(R.id.compensation);
        boolean isCheckedCompensation = compensation.isChecked();
        //checkbox for calm
        CheckBox calm = (CheckBox) findViewById(R.id.calm);
        boolean isCheckedCalm = calm.isChecked();
        //checkbox for ignore
        CheckBox ignore = (CheckBox) findViewById(R.id.ignore);
        boolean isCheckedIgnore = ignore.isChecked();
        //checkbox for vehicle_condition
        CheckBox vehicle_condition = (CheckBox) findViewById(R.id.vehicle_condition);
        boolean isCheckedVehicle_condition = vehicle_condition.isChecked();
        //checkbox for insurance
        CheckBox insurance = (CheckBox) findViewById(R.id.insurance);
        boolean isCheckedInsurance = insurance.isChecked();
        //checkbox for call_help
        CheckBox call_help = (CheckBox) findViewById(R.id.call_help);
        boolean isCheckedCall_help = call_help.isChecked();
        //checkbox for injured
        CheckBox injured = (CheckBox) findViewById(R.id.injured);
        boolean isCheckedInjured = injured.isChecked();
        //checkbox for move_vehicle
        CheckBox move_vehicle = (CheckBox) findViewById(R.id.move_vehicle);
        boolean isCheckedMove_vehicle = move_vehicle.isChecked();

        //checkbox points counting logic
        if (
            //check if all proper checkboxes are true
                injured.isChecked() &&
                call_help.isChecked() &&
                calm.isChecked() &&
                move_vehicle.isChecked() &&
            //and all the other are false
                !isCheckedInsurance &&
                !isCheckedVehicle_condition &&
                !isCheckedIgnore &&
                !isCheckedCompensation
            ){ checkBoxPoints = 1; } //if it all turns true value, award it with 1 point
        return checkBoxPoints;
    }

    public void submitQuiz(View view) {
        //calculate the points and send result to the screen
       total_points =  getCheckBoxPoints() + getRadioPoints(view) + getTextPoints(view);
        displayMessage(quizSummary());
    }

    public String quizSummary(){
        //check the results if all went fine you are prepared for real exam
        String summaryMessage = total_points + getString(R.string.max_value);
        if (total_points == 5){ summaryMessage = summaryMessage + getString(R.string.perfect);}
        else { summaryMessage = summaryMessage + getString(R.string.keep_working);}
        return summaryMessage;
    }

    public void displayMessage(String message){
        //display result on the screen
        TextView scoreMessage = (TextView) findViewById(R.id.score_message);
        scoreMessage.setText(message);
        //display toast with score and congrats
        Toast.makeText(this, getString(R.string.result) +quizSummary(), Toast.LENGTH_LONG).show();
    }
}